json.extract! product, :id, :name, :description, :price, :available_from, :created_at, :updated_at
json.url product_url(product, format: :json)
